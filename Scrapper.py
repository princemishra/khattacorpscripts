__author__ = 'prince'

import requests
import json
import os
import sys
import time
from sqlobject import *
import sqlite3 as sqlite

class BaseScrapper(object):
    """
    This class is purely virtual.
    """

    def __init__(self, page_id, access_token, max_pages = 1):
        self.page_id        = page_id
        self.max_pages      = max_pages
        self.access_token   = access_token
        self.cur_page       = 0

    def get_url_content(self, url):
        r           = requests.get(url)
        text        = r.text
        json_text   = json.loads(text)
        return text, json_text

    def make_url(self, paging_url = ''):
        if paging_url:
            url = paging_url
        else:
            url = 'https://graph.facebook.com/' + self.page_id + '/feed/?access_token=' + self.access_token
        return url

    def consume_data(self, text, json_text):
        """
        To be implemented
        """
        pass

    def get_data(self):
        url                 = self.make_url()
        text, json_text     = self.get_url_content(url)
        self.cur_page += 1
        self.consume_data(text, json_text)
        while self.cur_page < self.max_pages:
            self.cur_page += 1
            paging_url = json_text.get('paging')
            if paging_url:
                next_url = paging_url.get('next')
                if next_url:
                    url = self.make_url(next_url)
                    text, json_text     = self.get_url_content(url)
                    self.consume_data(text, json_text)


class FileScrapper(BaseScrapper):

    def __init__(self, page_id, access_token, folder_path, file_prefix = '', max_pages = 1):
        self.folder_path = os.path.abspath(folder_path)
        if not os.path.exists(self.folder_path):
            raise Exception("Folder does not exist.")
        self.file_prefix = file_prefix or page_id
        super(FileScrapper, self).__init__(page_id, access_token, max_pages)

    def consume_data(self, text, json_text):
        print "Consuming : ", self.cur_page
        # do something with the data
        full_path = os.path.join(self.folder_path, self.file_prefix + '_' + str(self.cur_page) + '.json')
        if os.path.exists(full_path):
            raise Exception("This file exists already.")
        f = open(full_path, 'w+')
        f.write(text)
        f.close()


class Khatta(SQLObject):
    message     = UnicodeCol(length = 9999, unique = True) # lets ensure unique links at db level
    likes       = IntCol(default=-1)
    comments    = IntCol(default=-1)
    created_on  = IntCol(default=0)
    author_id   = IntCol(default=-1)
    author_name = UnicodeCol(length=100, default='me')
    post_id     = UnicodeCol(default='')


class DBScrapper(BaseScrapper):

    def __init__(self, page_id, access_token, db_path, max_pages = 1):
        self.db_path = os.path.abspath(db_path)
        if not os.path.exists(self.db_path):
            raise Exception("The folder specified does not exist.")
        super(DBScrapper, self).__init__(page_id, access_token, max_pages)
        self.connection_string = 'sqlite:' + os.path.join(self.db_path, page_id + '-export.db')
        self.connect_db()
        self.init_db()

    def connect_db(self):
        connection = connectionForURI(self.connection_string)
        sqlhub.processConnection = connection

    def init_db(self):
        Khatta.createTable(ifNotExists=True)

    def consume_data(self, text, json_text):
        print "Consuming : ", self.cur_page
        for item in json_text['data']:
            try:
                message     = item.get('message')
                likes       = item.get('likes')
                comments    = item.get('comments')
                likes_count = 0
                comments_count = 0
                if message:
                    msg = message.encode('utf-8')
                    if likes:
                        try:
                            likes_count = likes['count']
                        except Exception, fault:
                            print "Like fault "
                            print str(fault), item
                    if comments:
                        try:
                            comments_count = len(comments['data'])
                        except Exception, fault:
                            print "Comment fault "
                            print str(fault), item
                    post_id     = item['id']
                    author_name = item['from']['name']
                    author_id   = int(item['from']['id'])
                    created_on  = int(time.mktime(time.strptime(item['created_time'].split('+')[0], '%Y-%m-%dT%H:%M:%S')))
                    nid         = Khatta(message=message, likes=likes_count, comments=comments_count, created_on=created_on, author_name=author_name, author_id=author_id, post_id=post_id)
                    print "nid", nid.id, likes_count
            except Exception, fault:
                print "\n\n\nError : ####", str(fault)
                print "item : ", item
                continue

    def insert_posts(self, base_dir):
        bdir = os.path.join(base_dir)
        all_items = os.listdir(bdir)
        for item in all_items:
            p = os.path.join(bdir, item)
            try:
                print "\n\nprocessing : %s" % p
                f = open(p, 'r')
                data = f.read()
                f.close()
                j = json.loads(data)
                #print j['data'][0]['message']
                for i in j['data']:
                    try:
                        message     = i['message'].encode('utf-8')
                        has_likes   = i.get('likes')
                        likes       = int(has_likes['count']) if has_likes else 0
                        has_comments= i.get('comments')
                        comments    = int(has_comments['count']) if has_comments else 0
                        post_id     = i['id']
                        author_name = i['from']['name']
                        author_id   = int(i['from']['id'])
                        created_on  = int(time.mktime(time.strptime(i['created_time'].split('+')[0], '%Y-%m-%dT%H:%M:%S')))
                        nid         = Khatta(message=message, likes=likes, comments=comments, created_on=created_on, author_name=author_name, author_id=author_id, post_id=post_id)
                        print "nid", nid
                    except Exception, e:
                        print "not sent", str(e)
            except Exception, fault:
                print "\n\n\nnot open %s" % str(fault)

if __name__=="__main__":
    """try:
        script, page_id, folder_path, file_prefix, max_pages, access_token = sys.argv
    except Exception, fault:
        sys.exit("Usage : python Scrapper.py page_id full_folder_path file_prefix max_pages access_token")
    sc = FileScrapper(page_id, access_token, folder_path, file_prefix, int(max_pages))
    sc.get_data()"""

    try:
        script, page_id, folder_path, db_path, access_token, max_pages= sys.argv
    except Exception, fault:
        sys.exit("Usage : python Scrapper.py page_id full_folder_path full_db_path access_token")
    sc = DBScrapper(page_id, access_token, db_path, max_pages)
    #sc.insert_posts(folder_path)
    sc.get_data()
